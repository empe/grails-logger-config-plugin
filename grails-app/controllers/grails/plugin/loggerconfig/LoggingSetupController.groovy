package grails.plugin.loggerconfig

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger
import org.apache.log4j.Appender;
import grails.plugin.loggerconfig.LoggerFilter;

class LoggingSetupController {

	def index = { redirect(action: list, params: params) }
	
	def list = {
		checkValidConfiguration()
		def loggerFilter
		if (params.filteringEnabled){
			flash.message = "Filtering enabled."
			loggerFilter = new LoggerFilter()
			bindData(loggerFilter, params)
		} else {
			loggerFilter = LoggerFilter.defaultFilter();
		}

		def loggers = getFilteredLoggersList(loggerFilter);
		
		loggers.sort { a,b ->
			a.getName() <=> b.getName() 			
		}
		
		[loggers : loggers, loggerFilter : loggerFilter, params : params]
	}
	
	/** Tests if there is any appender defined for <b>root logger</b> */
	private def checkValidConfiguration = {
		def rootAppenders = LogManager.getRootLogger()?.getAllAppenders()
		if (rootAppenders == null || !rootAppenders.hasMoreElements())
			flash.warning = "No appenders defined for root logger. Logging is disabled."
	}
	
	/** Retrieves list of {@link Logger}s specified by given {@link LoggerFilter}. */
	private def getFilteredLoggersList(LoggerFilter loggerFilter) {
		def allLoggersList = getAllLoggersList()
		def loggersList = allLoggersList.findAll { logger ->
			loggerFilter.matches(logger)
		}
		return loggersList
	}

	
	/** Retrieves list of all active Log4j {@link Logger}s */
	private def getAllLoggersList() {
		def loggersEnumeration = LogManager.getCurrentLoggers();
		def loggers = Collections.list(loggersEnumeration);
		return loggers
	}
	
	def changeLevel = {
		Logger logger = LogManager.getLogger(params.loggerName)
		Level level = Level.toLevel(params.loggerLevel);
		if (logger && level){
			if (level == Level.OFF.toString())
				logger.setLevel(Level.OFF);
			else 
				logger.setLevel(level)
				
			flash.message="Logger ${logger?.getName()} level set to ${level?.toString()}"
		}
		redirect(action:list)
		return
	}

	/** Returns list of all available logging levels */
	static def getLevelsList (){
		return [
			Level.OFF.toString(),
			Level.ERROR.toString(),
			Level.WARN.toString(),
			Level.INFO.toString(),
			Level.DEBUG.toString(),
			Level.TRACE.toString()
		]
	}
}
