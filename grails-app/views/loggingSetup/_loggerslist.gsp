<div>
<table width="20%">
	<thead>
		<tr>
			<th>Logger Name</th>
			<th>Log Level</th>
		</tr>
	</thead>
	<tbody>
		<g:each in="${loggers}" status="i" var="logger">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
				<td>
					${fieldValue(bean:logger, field:"name")}
				</td>
				<td>
					${fieldValue(bean:logger, field:"level")}
				</td>
			</tr>
		</g:each>
	</tbody>
</table>
</div>
