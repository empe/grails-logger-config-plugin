<%@page	import="grails.plugin.loggerconfig.LoggingSetupController"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="main" />
	<title>Logger Configuration</title>
	
	<style type="text/css">
 		.levelFilters li {
 			display:inline;
 			margin-right: 15px;
 		}
 	</style>
</head>



<body>
	<div class="nav">
		<span class="menuButton"> <a class="home"
			href="${createLink(uri: '/')}">Home</a>
		</span>
	</div>
	<div class="body">
		<h1>LOG4J Logging Configuration</h1>
		<g:if test="${flash.message}">
			<div class="message">
				${flash.message}
			</div>
		</g:if>
		<g:if test="${flash.warning}">
			<div class="errors">
				<ul>
					<li>${flash.warning}</li>
				</ul>
			</div>
		</g:if>
		<div class="dialog" style="margin-bottom: 20px;">
			<g:form controller="loggingSetup" action="list">
				<g:hiddenField name="filteringEnabled" value="${true}]"/>
				<table>
					<tr class="prop">
						<td valign="top" class="name">
							<label for="levelFilters">Level Filters:</label>
						</td>
						<td valign="top" class="value" width="500">
							<ul style="list-style-type: none; " class="levelFilters">
								<li><g:checkBox name="showError" value="${loggerFilter.showError}" />ERROR</li>
								<li><g:checkBox name="showWarn" value="${loggerFilter.showWarn}" />WARN</li>
								<li><g:checkBox name="showInfo" value="${loggerFilter.showInfo}" />INFO</li>
								<li><g:checkBox name="showDebug" checked="${loggerFilter.showDebug}" />DEBUG</li>
								<li><g:checkBox name="showTrace" checked="${loggerFilter.showTrace}" />TRACE</li>
								<li><g:checkBox name="showOffline" value="${loggerFilter.showOffline}" />OFF</li>
							</ul>
						</td>
					</tr>
					<tr class="prop">
						<td valign="top" class="name">
							<label for="namePattern"  >Name pattern:</label>
						</td>
						<td>
							<input type="text" style="width: 100%;" id="namePattern" name="namePattern" value="${fieldValue(bean:loggerFilter,field:'namePattern')}" />
						</td>
					</tr>
				</table>
				<div class="buttons">
					<span class="button">
						<g:actionSubmit class="edit" value="Update View" action="list" />
					</span>
				</div>
			</g:form>
		</div>
		<div class="dialog" style="margin-bottom: 20px;">
			<div id="logger-list">
				<g:render template="loggerslist" model="${[loggers : loggers]}"
					plugin="logger-config" />
			</div>
		</div>
		<div>
			<div class="dialog" id="logger-level-change" >
				<table>
					<g:form method="post">
					<tr class="prop">
						<td valign="top" class="name">
							<g:select name="loggerName" style="width: 100%;" from="${loggers}" optionValue="name" optionKey="name" />
						</td>
						<td valign="top" class="value">
							<g:select name="loggerLevel" from="${LoggingSetupController.getLevelsList()}" />
						</td>
						<td valign="top" class="value">
							<g:actionSubmit class="edit" style="width: 80px;" value="Change" action="changeLevel"/>
						</td>
					</tr>
					</g:form>
					<g:form method="post">
					<tr class="prop">
						<td valign="top" class="name">
							<input style="width: 100%;" type="text" name="loggerName" />
						</td>
						<td valign="top" class="value">
							<g:select name="loggerLevel" from="${LoggingSetupController.getLevelsList()}" />
						</td>
						<td valign="top" class="value">
							<g:actionSubmit class="create" style="width: 80px;" value="Add" action="changeLevel"/>
						</td>
					</tr>
					</g:form>
				</table>			
			</div>
		</div>
	</div>
</body>
</html>