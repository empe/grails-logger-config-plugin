class LoggerConfigGrailsPlugin {
    
    def version = "0.1"
    
    def grailsVersion = "2.0 > *"
    
    def dependsOn = [:]
    
    def pluginExcludes = [
            "grails-app/views/error.gsp",
			"web-app/css/errors.css"
    ]

    def author = "Mateusz Pytel"
    def authorEmail = "mateusz.pytel.87@gmail.com"
    
	def title = "Web UI for Log4J configuration"
	
    def description = "Plugin provides web UI to modify Log4J logger configuration at runtime."
    def license = "APACHE"
	def issueManagement = [ system: "GitHub", url: "https://github.com/e-m-p/grails-logger-config-plugin/issues" ]
	def documentation = ""
	
	
// Any additional developers beyond the author specified above.
//    def developers = [ [ name: "Joe Bloggs", email: "joe@bloggs.net" ]]

		// Online location of the plugin's browseable source code.
//    def scm = [ url: "http://svn.grails-plugins.codehaus.org/browse/grails-plugins/"


    // URL to the plugin's documentation
   

    def doWithWebDescriptor = { xml ->
        // TODO Implement additions to web.xml (optional), this event occurs before 
    }

    def doWithSpring = {
        // TODO Implement runtime spring config (optional)
    }

    def doWithDynamicMethods = { ctx ->
        // TODO Implement registering dynamic methods to classes (optional)
    }

    def doWithApplicationContext = { applicationContext ->
        // TODO Implement post initialization spring config (optional)
    }

    def onChange = { event ->
        // TODO Implement code that is executed when any artefact that this plugin is
        // watching is modified and reloaded. The event contains: event.source,
        // event.application, event.manager, event.ctx, and event.plugin.
    }

    def onConfigChange = { event ->
        // TODO Implement code that is executed when the project configuration changes.
        // The event is the same as for 'onChange'.
    }
}
