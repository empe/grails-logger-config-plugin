package grails.plugin.loggerconfig;

import grails.plugin.loggerconfig.LoggerFilter;

import java.util.regex.Pattern;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/** Configuration class for active loggers view 
 *  for filtering by level and logger name pattern */
public class LoggerFilter {
	
	public boolean showError;
	public boolean showWarn;
	public boolean showInfo;
	public boolean showDebug;
	public boolean showTrace;
	public boolean showOffline;
	
	public String namePattern;
	
	/**
	 * Factory method for filter which creates default
	 * logger filter, for showing only online loggers 
	 * without filtering by name
	 * 
	 * @return {@link LoggerFilter} instance with default configuration
	 */
	public static LoggerFilter defaultFilter(){
		LoggerFilter defaultFilter = new LoggerFilter();
		defaultFilter.showError = true;
		defaultFilter.showWarn = true;
		defaultFilter.showInfo = true;
		defaultFilter.showDebug = true;
		defaultFilter.showTrace = true;
		defaultFilter.showOffline = false;
		return defaultFilter;
	}
	
	/**
	 * Checks if given {@link Logger} instance
	 * matches this filter.
	 * @param logger {@link Logger} instance to check
	 * @return true if given instance matches specified settings, false otherwise
	 */
	public boolean matches(Logger logger){

		if (namePattern != null && !namePattern.isEmpty()){
			if (!Pattern.matches(".*"+namePattern+".*", logger.getName()))
				return false;
		}
		
		Level currentLevel = logger.getLevel();
		
		if (currentLevel == null){
			if (showOffline)
				return true;
			else 
				return false;
		}
		
		if (currentLevel.equals(Level.ERROR) && showError)
			return true;
		
		if (currentLevel.equals(Level.WARN) && showWarn)
			return true;
		
		if (currentLevel.equals(Level.INFO) && showInfo)
			return true;
		
		if (currentLevel.equals(Level.DEBUG) && showDebug)
			return true;
		
		if (currentLevel.equals(Level.TRACE) && showTrace)
			return true;
		
		if (currentLevel.equals(Level.OFF) && showOffline)
			return true;

		return false;
	}

	public boolean isShowError() {
		return showError;
	}

	public void setShowError(boolean showError) {
		this.showError = showError;
	}

	public boolean isShowWarn() {
		return showWarn;
	}

	public void setShowWarn(boolean showWarn) {
		this.showWarn = showWarn;
	}

	public boolean isShowInfo() {
		return showInfo;
	}

	public void setShowInfo(boolean showInfo) {
		this.showInfo = showInfo;
	}

	public boolean isShowDebug() {
		return showDebug;
	}

	public void setShowDebug(boolean showDebug) {
		this.showDebug = showDebug;
	}

	public boolean isShowTrace() {
		return showTrace;
	}

	public void setShowTrace(boolean showTrace) {
		this.showTrace = showTrace;
	}

	public boolean isShowOffline() {
		return showOffline;
	}

	public void setShowOffline(boolean showOffline) {
		this.showOffline = showOffline;
	}

	
	public String getNamePattern() {
		return namePattern;
	}

	public void setNamePattern(String namePattern) {
		this.namePattern = namePattern;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (showDebug ? 1231 : 1237);
		result = prime * result + (showError ? 1231 : 1237);
		result = prime * result + (showInfo ? 1231 : 1237);
		result = prime * result + (showOffline ? 1231 : 1237);
		result = prime * result
				+ ((namePattern == null) ? 0 : namePattern.hashCode());
		result = prime * result + (showTrace ? 1231 : 1237);
		result = prime * result + (showWarn ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LoggerFilter other = (LoggerFilter) obj;
		if (showDebug != other.showDebug)
			return false;
		if (showError != other.showError)
			return false;
		if (showInfo != other.showInfo)
			return false;
		if (showOffline != other.showOffline)
			return false;
		if (namePattern == null) {
			if (other.namePattern != null)
				return false;
		} else if (!namePattern.equals(other.namePattern))
			return false;
		if (showTrace != other.showTrace)
			return false;
		if (showWarn != other.showWarn)
			return false;
		return true;
	}
	

}
