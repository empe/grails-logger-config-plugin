**grails-logger-config-plugin** is a [Grails](http://grails.org) plugin which provides web UI to modify Log4J logger configuration at runtime.
Project documentation can be found [HERE](http://e-m-p.github.com/grails-logger-config-plugin/).

Enjoy!